package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.brew.Brew;
import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryManager;

public class ModRegistries
{
    public static void createRegistries(RegistryEvent.NewRegistry event)
    {
        RegistryBuilder<Brew> brewRegistryBuilder = new RegistryBuilder<>();

        brewRegistryBuilder.setType(Brew.class);

        ResourceLocation brewKey = new ResourceLocation(Herblore.MOD_ID, "brews");
        brewRegistryBuilder.setName(brewKey);
        brewRegistryBuilder.setDefaultKey(brewKey);

        brewRegistryBuilder.create();


        RegistryBuilder<BrewEffect> brewEffectRegistryBuilder = new RegistryBuilder<>();

        brewEffectRegistryBuilder.setType(BrewEffect.class);

        ResourceLocation brewEffectKey = new ResourceLocation(Herblore.MOD_ID, "brew_effects");
        brewEffectRegistryBuilder.setName(brewEffectKey);
        brewEffectRegistryBuilder.setDefaultKey(brewEffectKey);

        brewEffectRegistryBuilder.create();
    }
}
