package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.block.BlockBrewmasterApparatus;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModBlocks
{
    public static final DeferredRegister<Block> BLOCK_REGISTER = DeferredRegister.create(ForgeRegistries.BLOCKS, Herblore.MOD_ID);

    public static final RegistryObject<BlockBrewmasterApparatus> BREWMASTER_APPARATUS = BLOCK_REGISTER.register("brewmaster_apparatus", BlockBrewmasterApparatus::new);

    public static final RegistryObject<Block> CARNALLITE_ORE = BLOCK_REGISTER.register("carnallite_ore",
            () -> new Block(AbstractBlock.Properties.from(Blocks.IRON_ORE)));

    public static final RegistryObject<Block> OAKBLOOD_PLANT = BLOCK_REGISTER.register("oakblood_plant",
            () -> new Block(AbstractBlock.Properties.create(Material.PLANTS).doesNotBlockMovement().zeroHardnessAndResistance().sound(SoundType.PLANT)));

    public static final RegistryObject<Block> ARROWHEAD_LEAVES_PLANT = BLOCK_REGISTER.register("arrowhead_leaves_plant",
            () -> new Block(AbstractBlock.Properties.create(Material.PLANTS).doesNotBlockMovement().zeroHardnessAndResistance().sound(SoundType.PLANT)));
}
