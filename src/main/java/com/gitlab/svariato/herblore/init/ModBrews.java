package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.brew.Brew;
import com.gitlab.svariato.herblore.common.Herblore;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;

public class ModBrews
{
    public static final DeferredRegister<Brew> BREW_REGISTER = DeferredRegister.create(Brew.class, Herblore.MOD_ID);

    public static final RegistryObject<Brew> CUSTOM_BREW = BREW_REGISTER.register("custom_brew", () -> new Brew("custom", 3, 32));
}
