package com.gitlab.svariato.herblore.init;

import net.minecraft.entity.EntityType;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ITag;

public class ModTags
{
    public static final ITag.INamedTag<EntityType<?>> PHEROMONE_FOLLOWERS = EntityTypeTags.func_232896_a_("herblore:pheromone_followers");
}
