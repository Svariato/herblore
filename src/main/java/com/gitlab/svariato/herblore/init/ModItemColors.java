package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.common.util.BrewHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;

public class ModItemColors
{
    private static void init()
    {
        ItemColors itemColors = Minecraft.getInstance().getItemColors();

        itemColors.register((stack, t) -> t > 0 ? -1 : BrewHelper.getBrewColor(stack), ModItems.BREW.get());
    }

    public static void onLoadComplete(FMLLoadCompleteEvent event)
    {
        init();
    }
}
