package com.gitlab.svariato.herblore.init;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

import java.util.function.Supplier;

public class ModItemGroups
{
    public static final ItemGroup HERBLORE_ITEM_GROUP = new ModItemGroup("Herblore", () -> new ItemStack(Items.PUMPKIN));

    public static class ModItemGroup extends ItemGroup
    {
        private final Supplier<ItemStack> iconSupplier;

        public ModItemGroup(String name, Supplier<ItemStack> iconSupplier)
        {
            super(name);
            this.iconSupplier = iconSupplier;
        }

        @Override
        public ItemStack createIcon() { return iconSupplier.get(); }
    }
}
