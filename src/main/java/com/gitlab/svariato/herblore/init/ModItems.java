package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.item.ItemBasicAttuningReagent;
import com.gitlab.svariato.herblore.common.item.ItemBrew;
import com.gitlab.svariato.herblore.common.item.ItemDirtyHerb;
import com.gitlab.svariato.herblore.common.item.ItemHerb;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModItems
{
    public static final DeferredRegister<Item> ITEM_REGISTER = DeferredRegister.create(ForgeRegistries.ITEMS, Herblore.MOD_ID);

    public static final RegistryObject<Item> GLASS_VIAL = ITEM_REGISTER.register("glass_vial", () -> new Item(defaultProperties()));

    public static final RegistryObject<Item> BEESBALM = ITEM_REGISTER.register("beesbalm", () -> new ItemHerb(defaultProperties(), 6750003, new BrewInstance(200,
            new BrewEffectInstance(ModBrewEffects.GLIDE, 100))));
    public static final RegistryObject<Item> BULLWEED = ITEM_REGISTER.register("bullweed", () -> new ItemHerb(defaultProperties(), 16737792, new BrewInstance(200,
        new BrewEffectInstance(ModBrewEffects.PHEROMONE, 100))));
    public static final RegistryObject<Item> PIGFOOT = ITEM_REGISTER.register("pigfoot", () -> new ItemHerb(defaultProperties(), 6724095, new BrewInstance(200,
        new BrewEffectInstance(ModBrewEffects.WALL_CLIMB, 100))));
    public static final RegistryObject<Item> SILVERFIN = ITEM_REGISTER.register("silverfin", () -> new ItemHerb(defaultProperties(), 65484, new BrewInstance(200,
        new BrewEffectInstance(ModBrewEffects.KNOCKBACK_RESISTANCE, 100))));

    public static final RegistryObject<Item> ARROWHEAD_LEAVES = ITEM_REGISTER.register("arrowhead_leaves", () -> new ItemHerb(defaultProperties(), 10604443, new BrewInstance(1200,
            new BrewEffectInstance(ModBrewEffects.ATTACK_SPEED_BOOST, 20))));
    public static final RegistryObject<Item> OAKBLOOD = ITEM_REGISTER.register("oakblood", () -> new ItemHerb(defaultProperties(), 10230289, new BrewInstance(1200,
            new BrewEffectInstance(ModBrewEffects.HEALTH_REGENERATION, 20))));

    public static final RegistryObject<Item> DIRTY_BEESBALM = ITEM_REGISTER.register("dirty_beesbalm", () -> new ItemDirtyHerb(defaultProperties(), BEESBALM));
    public static final RegistryObject<Item> DIRTY_BULLWEED = ITEM_REGISTER.register("dirty_bullweed", () -> new ItemDirtyHerb(defaultProperties(), BULLWEED));
    public static final RegistryObject<Item> DIRTY_PIGFOOT = ITEM_REGISTER.register("dirty_pigfoot", () -> new ItemDirtyHerb(defaultProperties(), PIGFOOT));
    public static final RegistryObject<Item> DIRTY_SILVERFIN = ITEM_REGISTER.register("dirty_silverfin", () -> new ItemDirtyHerb(defaultProperties(), SILVERFIN));

    public static final RegistryObject<Item> DIRTY_ARROWHEAD_LEAVES = ITEM_REGISTER.register("dirty_arrowhead_leaves", () -> new ItemDirtyHerb(defaultProperties(), ARROWHEAD_LEAVES));
    public static final RegistryObject<Item> DIRTY_OAKBLOOD = ITEM_REGISTER.register("dirty_oakblood", () -> new ItemDirtyHerb(defaultProperties(), OAKBLOOD));

    public static final RegistryObject<Item> CARNALLITE = ITEM_REGISTER.register("carnallite", () -> new ItemBasicAttuningReagent(defaultProperties(), 15097672, 1, 1));

    public static final RegistryObject<Item> BREW = ITEM_REGISTER.register("brew", () -> new ItemBrew(defaultProperties()));

    public static final RegistryObject<BlockItem> BREWMASTER_APPARATUS = ITEM_REGISTER.register("brewmaster_apparatus", () -> new BlockItem(ModBlocks.BREWMASTER_APPARATUS.get(), defaultProperties()));

    public static final RegistryObject<BlockItem> CARNALLITE_ORE = ITEM_REGISTER.register("carnallite_ore", () -> new BlockItem(ModBlocks.CARNALLITE_ORE.get(), defaultProperties()));

    public static Item.Properties defaultProperties()
    {
        return new Item.Properties().group(ModItemGroups.HERBLORE_ITEM_GROUP);
    }
}
