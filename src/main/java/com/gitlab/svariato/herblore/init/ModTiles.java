package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.tile.TileBrewmasterApparatus;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModTiles
{
    public static final DeferredRegister<TileEntityType<?>> TILE_REGISTER = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, Herblore.MOD_ID);

    public static final RegistryObject<TileEntityType<TileBrewmasterApparatus>> BREWMASTER_APPARATUS = TILE_REGISTER.register(
            "brewmaster_apparatus", () -> TileEntityType.Builder.create(TileBrewmasterApparatus::new, ModBlocks.BREWMASTER_APPARATUS.get()).build(null));
}
