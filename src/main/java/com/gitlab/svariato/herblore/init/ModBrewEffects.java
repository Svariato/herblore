package com.gitlab.svariato.herblore.init;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.brew.*;
import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.potion.EffectType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;

public class ModBrewEffects
{
    public static final DeferredRegister<BrewEffect> BREW_EFFECT_REGISTER = DeferredRegister.create(BrewEffect.class, Herblore.MOD_ID);

    public static final RegistryObject<BrewEffect> HEALTH_REGENERATION = BREW_EFFECT_REGISTER.register("health_regeneration",
            () -> new BrewEffectHealthRegeneration());

    public static final RegistryObject<BrewEffect> MOVEMENT_SPEED_BOOST = BREW_EFFECT_REGISTER.register("movement_speed_boost",
            () -> new BrewEffectMovementSpeed("movement_speed_boost", EffectType.BENEFICIAL, "83e5a3fa-12aa-4f8f-a9c9-99ea5045c30c", AttributeModifier.Operation.MULTIPLY_TOTAL));

    public static final RegistryObject<BrewEffect> ATTACK_SPEED_BOOST = BREW_EFFECT_REGISTER.register("attack_speed_boost",
            () -> new BrewEffectAttackSpeed("attack_speed_boost", EffectType.BENEFICIAL, "4903cf7f-4e6f-4371-83c5-71702fe0f0a0", AttributeModifier.Operation.MULTIPLY_TOTAL));

    public static final RegistryObject<BrewEffect> JUMP_HEIGHT_BOOST = BREW_EFFECT_REGISTER.register("jump_height_boost",
            () -> new BrewEffect("Jump_height_boost", EffectType.BENEFICIAL));

    public static final RegistryObject<BrewEffect> SWIM_SPEED_BOOST = BREW_EFFECT_REGISTER.register("swim_speed_boost",
            () -> new BrewEffectSwimSpeed("swim_speed_boost", EffectType.BENEFICIAL, "a2a39592-a92e-435d-89de-0f4e1e140715", AttributeModifier.Operation.MULTIPLY_TOTAL));

    public static final RegistryObject<BrewEffect> WATER_BREATHING = BREW_EFFECT_REGISTER.register("water_breathing",
            () -> new BrewEffectWaterBreathing());

    public static final RegistryObject<BrewEffect> LIFE_STEAL = BREW_EFFECT_REGISTER.register("life_steal",
            () -> new BrewEffect("life_steal", EffectType.BENEFICIAL));

    public static final RegistryObject<BrewEffect> KNOCKBACK_RESISTANCE = BREW_EFFECT_REGISTER.register("knockback_resistance",
            () -> new BrewEffect("knockback_resistance", EffectType.BENEFICIAL));

    public static final RegistryObject<BrewEffect> KNOCKBACK_SUSCEPTIBILITY = BREW_EFFECT_REGISTER.register("knockback_susceptibility",
            () -> new BrewEffect("knockback_susceptibility", EffectType.HARMFUL));

    public static final RegistryObject<BrewEffect> GLIDE = BREW_EFFECT_REGISTER.register("glide",
            () -> new BrewEffectGlide());

    public static final RegistryObject<BrewEffect> WALL_CLIMB = BREW_EFFECT_REGISTER.register("wall_climb",
            () -> new BrewEffectWallClimb());

    public static final RegistryObject<BrewEffect> PHEROMONE = BREW_EFFECT_REGISTER.register("pheromone",
            () -> new BrewEffectPheromone());
}
