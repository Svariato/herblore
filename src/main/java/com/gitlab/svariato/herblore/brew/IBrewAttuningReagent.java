package com.gitlab.svariato.herblore.brew;

public interface IBrewAttuningReagent extends IBrewReagent
{
    float getPotencyMultiplier();

    float getDurationMultiplier();
}
