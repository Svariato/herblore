package com.gitlab.svariato.herblore.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import com.gitlab.svariato.herblore.init.ModBrews;
import com.google.common.collect.ImmutableList;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.List;

/**
 * Some herb combinations create predefined brews with special names and
 * effects. This class allows for the registration of those predefined brews.
 */
public class Brew extends ForgeRegistryEntry<Brew>
{
    private final String name;
    private final int initialDoses;
    private final int drinkTime;
    private final BrewInstance baseBrewInstance;

    public Brew(String name, int initialDoses, int drinkTime)
    {
        this.name = name;
        this.initialDoses = initialDoses;
        this.drinkTime = drinkTime;
        this.baseBrewInstance = null;
    }

    public Brew(String name, int initialDoses, int drinkTime, BrewInstance baseBrewInstance)
    {
        this.name = name;
        this.initialDoses = initialDoses;
        this.drinkTime = drinkTime;
        this.baseBrewInstance = baseBrewInstance;
    }

    public String getName() { return name; }

    public int getInitialDoses() { return initialDoses; }

    public int getDrinkTime() { return drinkTime; }

    public BrewInstance getBaseBrewInstance() { return baseBrewInstance; }

    public BrewInstance getBrewInstance(float potencyMultiplier, float durationMultiplier)
    {
        return new BrewInstance(Math.round(baseBrewInstance.getDuration() * durationMultiplier), potencyMultiplier, baseBrewInstance.getBrewEffectInstanceMap());
    }
}
