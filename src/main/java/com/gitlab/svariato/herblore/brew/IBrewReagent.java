package com.gitlab.svariato.herblore.brew;

public interface IBrewReagent
{
    int getBrewColorContribution();
}
