package com.gitlab.svariato.herblore.brew;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class BrewReagents
{
    private final Collection<IBrewEffectReagent> brewEffectReagents;
    private final IBrewAttuningReagent purifyingReagent;
    private final Collection<IBrewReagent> reagents;

    public BrewReagents(IBrewAttuningReagent purifyingReagent, IBrewEffectReagent... brewEffectReagents)
    {
        this.brewEffectReagents = Arrays.asList(brewEffectReagents);
        this.purifyingReagent = purifyingReagent;

        this.reagents = new ArrayList<>();
        this.reagents.addAll(this.brewEffectReagents);
        this.reagents.add(purifyingReagent);
    }

    public BrewReagents(ItemStack purifyingReagentStack, ItemStack... brewEffectReagentStacks)
    {
        this.brewEffectReagents = new ArrayList<>();
        for (ItemStack brewEffectReagentStack : brewEffectReagentStacks)
        {
            brewEffectReagents.add((IBrewEffectReagent)brewEffectReagentStack.getItem());
        }
        this.purifyingReagent = purifyingReagentStack != null ? (IBrewAttuningReagent)purifyingReagentStack.getItem() : null;

        this.reagents = new ArrayList<>();
        this.reagents.addAll(this.brewEffectReagents);
        this.reagents.add(purifyingReagent);
    }

    public Collection<IBrewEffectReagent> getBrewEffectReagents() { return brewEffectReagents; }

    public IBrewAttuningReagent getPurifyingReagent() { return purifyingReagent; }

    public Collection<IBrewReagent> getReagents() { return reagents; }
}
