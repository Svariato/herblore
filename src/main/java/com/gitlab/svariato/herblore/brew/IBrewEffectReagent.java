package com.gitlab.svariato.herblore.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewInstance;

public interface IBrewEffectReagent extends IBrewReagent
{
    public abstract BrewInstance getBaseBrewInstance();
}
