package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectType;
import net.minecraft.util.math.vector.Vector3d;

public class BrewEffectWallClimb extends BrewEffect
{
    public BrewEffectWallClimb()
    {
        super("wall_climb", EffectType.BENEFICIAL);
    }

    @Override
    public void tick(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        if (livingEntity.collidedHorizontally)
        {
            float climbingSpeed = getClimbingSpeed(potency);
            Vector3d motion = livingEntity.getMotion();

            livingEntity.fallDistance = 0;

            if (livingEntity.isCrouching())
                livingEntity.setMotion(motion.x, 0, motion.z);
            else if (livingEntity.moveForward > 0.0f && motion.y < climbingSpeed) // Don't slow down if vertical speed is already higher.
                livingEntity.setMotion(motion.x, climbingSpeed, motion.z);
        }
    }

    private static float getClimbingSpeed(int potency) { return 0.75f * potency / BrewEffectInstance.MAX_POTENCY; }
}
