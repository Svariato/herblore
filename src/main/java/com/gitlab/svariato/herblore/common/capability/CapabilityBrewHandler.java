package com.gitlab.svariato.herblore.common.capability;

import com.gitlab.svariato.herblore.common.Herblore;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;


public class CapabilityBrewHandler
{
    @CapabilityInject(IBrewHandler.class)
    public static Capability<IBrewHandler> BREW_HANDLER_CAPABILITY = null;

    public static void register()
    {
        CapabilityManager.INSTANCE.register(IBrewHandler.class, new CapabilityStorageBrewHandler(), BrewHandler::new);
    }

    public static void onAttachCapabilitiesEvent(AttachCapabilitiesEvent<Entity> event)
    {
        if (event.getObject() instanceof LivingEntity)
        {
            event.addCapability(new ResourceLocation(Herblore.MOD_ID, "brew_handler_capability"), new CapabilityProviderBrewHandler());
        }
    }
}
