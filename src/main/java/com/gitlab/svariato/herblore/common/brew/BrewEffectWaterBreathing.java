package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectType;
import net.minecraft.potion.Effects;

public class BrewEffectWaterBreathing extends BrewEffect
{
    private final EffectInstance waterBreathingTick = new EffectInstance(Effects.WATER_BREATHING, 1, 0, false, false, false);

    public BrewEffectWaterBreathing()
    {
        super("water_breathing", EffectType.BENEFICIAL);
    }

    @Override
    public void tick(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        livingEntity.addPotionEffect(waterBreathingTick);
    }
}
