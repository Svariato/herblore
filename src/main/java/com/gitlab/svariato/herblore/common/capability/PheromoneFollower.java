package com.gitlab.svariato.herblore.common.capability;

import net.minecraft.entity.LivingEntity;

public class PheromoneFollower implements IPheromoneFollower
{
    private LivingEntity target = null;

    @Override
    public LivingEntity getTarget()
    {
        return target;
    }

    @Override
    public void setTarget(LivingEntity target)
    {
        this.target = target;
    }
}
