package com.gitlab.svariato.herblore.common.goal;

import com.gitlab.svariato.herblore.common.brew.BrewEffectPheromone;
import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.CapabilityPheromoneFollower;
import com.gitlab.svariato.herblore.common.capability.IBrewHandler;
import com.gitlab.svariato.herblore.common.capability.IPheromoneFollower;
import com.gitlab.svariato.herblore.init.ModBrewEffects;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.passive.AnimalEntity;

import javax.annotation.Nonnull;
import java.util.EnumSet;

public class FollowPheromoneGoal extends Goal
{
    private final AnimalEntity animalEntity;
    private final IPheromoneFollower pheromoneFollowerCapability;
    private final double speed;
    private final float maxTargetDistance;

    public FollowPheromoneGoal(@Nonnull AnimalEntity animalEntity, double speedIn, float targetMaxDistance) {
        this.animalEntity = animalEntity;
        pheromoneFollowerCapability = animalEntity.getCapability(CapabilityPheromoneFollower.PHEROMONE_FOLLOWER_CAPABILITY).orElse(null);
        this.speed = speedIn;
        this.maxTargetDistance = targetMaxDistance;
        this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE));
    }

    /**
     * Returns whether execution should begin. You can also read and cache any state necessary for execution in this
     * method as well.
     */
    public boolean shouldExecute()
    {
        return targetHasInRangePheromoneEffect();
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {

        return !animalEntity.getNavigator().noPath() && pheromoneFollowerCapability.getTarget().isAlive() && targetHasInRangePheromoneEffect();
    }

    public void resetTask()
    {
        pheromoneFollowerCapability.setTarget(null);
        animalEntity.getNavigator().clearPath();
    }

    public void tick()
    {

        animalEntity.getLookController().setLookPositionWithEntity(pheromoneFollowerCapability.getTarget(), (float)(animalEntity.getHorizontalFaceSpeed() + 20), (float)animalEntity.getVerticalFaceSpeed());
        animalEntity.getNavigator().tryMoveToEntityLiving(pheromoneFollowerCapability.getTarget(), speed);
    }

    private boolean targetHasInRangePheromoneEffect()
    {
        if (pheromoneFollowerCapability.getTarget() != null)
        {
            IBrewHandler brewHandler = pheromoneFollowerCapability.getTarget().getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
            return brewHandler != null
                    && brewHandler.getActiveBrew() != null
                    && brewHandler.getActiveBrew().getBrewEffectInstanceMap().hasBrewInstanceOfEffect(ModBrewEffects.PHEROMONE)
                    && BrewEffectPheromone.isInRangeOfPheromone(animalEntity, pheromoneFollowerCapability.getTarget(),
                        brewHandler.getActiveBrew().getBrewEffectInstanceMap().getBrewInstanceOfEffect(ModBrewEffects.PHEROMONE).getPotency());
        }
        return false;
    }
}
