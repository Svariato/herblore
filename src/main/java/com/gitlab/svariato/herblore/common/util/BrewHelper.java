package com.gitlab.svariato.herblore.common.util;

import com.gitlab.svariato.herblore.brew.*;
import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstanceMap;
import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import com.gitlab.svariato.herblore.init.ModBrews;
import com.gitlab.svariato.herblore.init.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraft.util.text.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;

import javax.xml.soap.Text;
import java.util.List;
import java.util.function.Supplier;

/**
 * Maintains a collection of static functions that allow the user to
 * alter specific aspects of a Brew at runtime.
 */
public class BrewHelper
{
    private static final String BREW_TYPE_NBT_KEY = "herblore_brew";
    private static final String DOSES_REMAINING_NBT_KEY = "herblore_doses_remaining";
    private static final String TAGGED_BREW_INSTANCE_NBT_KEY = "herblore_tagged_brew_instance";
    private static final String COLOR_NBT_KEY = "herblore_brew_color";

    /**
     * NBT-decodes the ItemStack's tag, looking for a brew registry associated with it.
     */
    public static Brew getBrew(ItemStack stack)
    {
        if (stack.hasTag() && stack.getTag().contains(BREW_TYPE_NBT_KEY))
        {
            String brewID = stack.getTag().getString(BREW_TYPE_NBT_KEY);
            for (RegistryObject<Brew> e : ModBrews.BREW_REGISTER.getEntries())
            {
                return e.get();
            }
        }
        return ModBrews.CUSTOM_BREW.get(); // Default return.
    }

    /**
     * NBT-encodes the brew's registry location onto the given ItemStack for future use.
     */
    public static ItemStack setBrew(ItemStack stack, Brew brew)
    {
        ResourceLocation brewID = new ResourceLocation("Herblore", "custom");
        if (brew != null)
        {
            for (RegistryObject<Brew> e : ModBrews.BREW_REGISTER.getEntries())
            {
                if (e.get() == brew)
                {
                    brewID = e.getId();
                }
            }
        }
        stack.getOrCreateTag().putString(BREW_TYPE_NBT_KEY, brewID.toString());
        return stack;
    }

    public static int getDosesRemaining(ItemStack stack)
    {
        if (stack.hasTag())
        {
            if (stack.getTag().contains(DOSES_REMAINING_NBT_KEY))
            {
                return stack.getTag().getInt(DOSES_REMAINING_NBT_KEY);
            }
            else
            {
                int dosesRemaining = getBrew(stack).getInitialDoses();
                setDosesRemaining(stack, dosesRemaining);
                return dosesRemaining;
            }
        }
        return 0;
    }

    public static void setDosesRemaining(ItemStack stack, int dosesRemaining)
    {
        stack.getOrCreateTag().putInt(DOSES_REMAINING_NBT_KEY, dosesRemaining);
    }

    public static BrewInstance getTaggedBrewInstance(ItemStack stack)
    {
        if (!stack.getOrCreateTag().contains(TAGGED_BREW_INSTANCE_NBT_KEY)) return new BrewInstance(0); // TODO: Ensure this failsafe return is good enough.
        return BrewInstance.read(stack.getTag().getCompound(TAGGED_BREW_INSTANCE_NBT_KEY));
    }

    public static void setTaggedBrewInstance(ItemStack stack, BrewInstance brewInstance)
    {
        stack.getOrCreateTag().put(TAGGED_BREW_INSTANCE_NBT_KEY, brewInstance.write(new CompoundNBT()));
    }

    public static int getBrewColor(ItemStack stack)
    {
        if (stack.hasTag() && stack.getTag().contains(COLOR_NBT_KEY))
        {
            return stack.getTag().getInt(COLOR_NBT_KEY);
        }
        return 0;
    }

    public static void setBrewColor(ItemStack stack, int color)
    {
        stack.getOrCreateTag().putInt(COLOR_NBT_KEY, color);
    }

    public static int makeBrewColor(BrewReagents reagents)
    {
        int r = 0, g = 0, b = 0; // Initialize color information.
        for (IBrewReagent reagent : reagents.getReagents())
        {
            if (reagent != null) // TODO: This null check shouldn't be necessary after reagents work properly.
            {
                int ingredientColor = reagent.getBrewColorContribution();
                if (ingredientColor >= 0)
                {
                    r += (ingredientColor >> 16) & 255;
                    g += (ingredientColor >> 8) & 255;
                    b += ingredientColor & 255;
                }
            }
        }
        return (r << 16) | (g << 8) | (b);
    }

    public static ItemStack makeBrew(float cookTimeMultiplier, BrewReagents reagents)
    {
        ItemStack brewStack = new ItemStack(ModItems.BREW.get(), 1);

        // TODO: Add check for predefined recipes.

        // Make it into a custom brew since there is no predefined recipe.

        // Calculate multipliers from purifying reagent. If there isn't one, make the multipliers 0.5.
        IBrewAttuningReagent purifyingReagent = reagents.getPurifyingReagent();
        float potencyMultiplier = purifyingReagent != null ? purifyingReagent.getPotencyMultiplier() : 0.5f;
        float durationMultiplier = purifyingReagent != null ? purifyingReagent.getDurationMultiplier() : 0.5f;

        potencyMultiplier *= cookTimeMultiplier;
        durationMultiplier *= cookTimeMultiplier;

        // Create container of BrewEffectInstances that will be used to make the BrewInstance that gets attached to the Brew.
        BrewEffectInstanceMap brewEffectInstanceMap = new BrewEffectInstanceMap();

        int duration = 0;

        for (IBrewEffectReagent reagent : reagents.getBrewEffectReagents())
        {
            duration += reagent.getBaseBrewInstance().getDuration();

            // Iterate over every base BrewEffect in each effect reagent and add it to the map.
            for (BrewEffectInstance baseBrewEffectInstance : reagent.getBaseBrewInstance().getBrewEffectInstanceMap().getBrewEffectInstances())
            {
                BrewEffectInstance clonedBrewEffectInstance = baseBrewEffectInstance.cloneInstance();
                // Check if the same BrewEffect already exists in this brew. If it does, combine the effects!
                Supplier<BrewEffect> brewEffect = clonedBrewEffectInstance.getBrewEffect();
                if (brewEffectInstanceMap.hasBrewInstanceOfEffect(brewEffect))
                {
                    brewEffectInstanceMap.addBrewEffectInstance(clonedBrewEffectInstance.combine(brewEffectInstanceMap.removeBrewEffect(brewEffect)));
                }
                // If not, just add the cloned instance.
                else brewEffectInstanceMap.addBrewEffectInstance(clonedBrewEffectInstance);
            }
        }

        duration = Math.round(((float)duration) / ((float)reagents.getBrewEffectReagents().size()) * durationMultiplier);

        setBrewColor(brewStack, makeBrewColor(reagents));
        setTaggedBrewInstance(brewStack, new BrewInstance(duration, potencyMultiplier, brewEffectInstanceMap));

        Herblore.LOGGER.debug("Made brew with potency multiplier " + potencyMultiplier + " and duration multiplier " + durationMultiplier);

        return brewStack;
    }

    // Modified version of PotionUtils.addPotionTooltip() from vanilla.
    @OnlyIn(Dist.CLIENT)
    public static void addBrewTooltip(ItemStack stack, List<ITextComponent> lores)
    {
        // Set dosage information.
        int dosesRemaining = getDosesRemaining(stack);
        if (dosesRemaining == 1) lores.add((new TranslationTextComponent("1 Dose")).mergeStyle(TextFormatting.BLUE));
        else lores.add((new TranslationTextComponent(dosesRemaining + " Doses")).mergeStyle(TextFormatting.BLUE));

        BrewInstance brewInstance = getTaggedBrewInstance(stack);

        lores.add(new TranslationTextComponent("Duration").appendString(": (" + StringUtils.ticksToElapsedTime(brewInstance.getDuration()) + ")").mergeStyle(TextFormatting.AQUA));

        for (BrewEffectInstance brewEffectInstance : brewInstance.getBrewEffectInstanceMap().getBrewEffectInstances())
        {
            BrewEffect brewEffect = brewEffectInstance.getBrewEffect().get();
            TextFormatting effectTextColor = TextFormatting.GRAY;
            switch (brewEffect.getEffectType())
            {
                case BENEFICIAL:
                    effectTextColor = TextFormatting.GREEN;
                    break;
                case HARMFUL:
                    effectTextColor = TextFormatting.RED;
                    break;
                case NEUTRAL:
                    effectTextColor = TextFormatting.WHITE;
                    break;
            }
            lores.add(new TranslationTextComponent(brewEffect.getTranslationKey()).appendString(" (" + brewEffectInstance.getPotency() + ")").mergeStyle(effectTextColor));
        }
    }
}
