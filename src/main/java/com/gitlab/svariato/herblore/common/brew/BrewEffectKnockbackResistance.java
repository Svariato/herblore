package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.IBrewHandler;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.init.ModBrewEffects;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;

public class BrewEffectKnockbackResistance
{
    public static void onLivingKnockback(LivingKnockBackEvent event)
    {
        LivingEntity livingEntity = event.getEntityLiving();
        IBrewHandler brewHandler = livingEntity.getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
        if (brewHandler != null && brewHandler.getActiveBrew() != null)
        {
            if (brewHandler.getActiveBrew().getBrewEffectInstanceMap().hasBrewInstanceOfEffect(ModBrewEffects.KNOCKBACK_RESISTANCE))
            {
                BrewEffectInstance brewEffectInstance = brewHandler.getActiveBrew().getBrewEffectInstanceMap().getBrewInstanceOfEffect(ModBrewEffects.KNOCKBACK_RESISTANCE);
                event.setStrength(event.getStrength() * (1f - ((float)(brewEffectInstance.getPotency())) / ((float)(BrewEffectInstance.MAX_POTENCY)))); // Max potency means no knockback.
            }
            if (brewHandler.getActiveBrew().getBrewEffectInstanceMap().hasBrewInstanceOfEffect(ModBrewEffects.KNOCKBACK_SUSCEPTIBILITY))
            {
                BrewEffectInstance brewEffectInstance = brewHandler.getActiveBrew().getBrewEffectInstanceMap().getBrewInstanceOfEffect(ModBrewEffects.KNOCKBACK_SUSCEPTIBILITY);
                event.setStrength(event.getStrength() * (1f + ((float)(brewEffectInstance.getPotency())) / ((float)(BrewEffectInstance.MAX_POTENCY)))); // Max potency means twice the knockback.
            }
        }
    }
}
