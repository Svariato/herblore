package com.gitlab.svariato.herblore.common.tentative;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraftforge.common.util.Constants;

import javax.annotation.Nonnull;
import java.util.Collection;

public class BrewInstance
{
    public static final String DURATION_NBT_KEY = "herblore_duration";
    public static final String BREW_EFFECT_INSTANCE_LIST_NBT_KEY = "herblore_brew_effect_instance_list";

    private int duration = 0;

    private final BrewEffectInstanceMap brewEffectInstanceMap;

    public BrewInstance(int duration, float potencyMultiplier, BrewEffectInstance... effects)
    {
        this.duration = duration;

        brewEffectInstanceMap = new BrewEffectInstanceMap();
        for (BrewEffectInstance effect : effects)
            brewEffectInstanceMap.addBrewEffectInstance(new BrewEffectInstance(effect.getBrewEffect(), Math.round(effect.getPotency() * potencyMultiplier)));
    }

    public BrewInstance(int duration, float potencyMultiplier, BrewEffectInstanceMap effects)
    {
        this.duration = duration;

        brewEffectInstanceMap = new BrewEffectInstanceMap();
        for (BrewEffectInstance effect : effects.getBrewEffectInstances())
            brewEffectInstanceMap.addBrewEffectInstance(new BrewEffectInstance(effect.getBrewEffect(), Math.round(effect.getPotency() * potencyMultiplier)));
    }

    public BrewInstance(int duration, BrewEffectInstance... effects)
    {
        this(duration, 1, effects);
    }

    public BrewInstance(int duration, BrewEffectInstanceMap effects)
    {
        this(duration, 1, effects);
    }

    @Nonnull
    public BrewInstance cloneInstance()
    {
        return new BrewInstance(duration, 1, brewEffectInstanceMap);
    }

    public BrewEffectInstanceMap getBrewEffectInstanceMap() { return brewEffectInstanceMap; }

    public int getDuration() { return duration; }
    public void decrementDuration() { duration -= 1; }

    public CompoundNBT write(CompoundNBT nbt)
    {
        nbt.putInt(DURATION_NBT_KEY, duration);

        ListNBT nbtList;
        if (nbt.contains(BREW_EFFECT_INSTANCE_LIST_NBT_KEY, Constants.NBT.TAG_LIST)) nbtList = nbt.getList(BREW_EFFECT_INSTANCE_LIST_NBT_KEY, Constants.NBT.TAG_COMPOUND);
        else nbtList = new ListNBT();

        for (BrewEffectInstance effect : brewEffectInstanceMap.getBrewEffectInstances()) nbtList.add(effect.write(new CompoundNBT()));

        nbt.put(BREW_EFFECT_INSTANCE_LIST_NBT_KEY, nbtList);

        return nbt;
    }

    public static BrewInstance read(CompoundNBT nbt)
    {
        int taggedDuration = nbt.getInt(DURATION_NBT_KEY);

        BrewEffectInstanceMap effects = new BrewEffectInstanceMap();
        ListNBT nbtListEffects = nbt.getList(BREW_EFFECT_INSTANCE_LIST_NBT_KEY, Constants.NBT.TAG_COMPOUND);
        for (INBT tag : nbtListEffects) effects.addBrewEffectInstance(BrewEffectInstance.read((CompoundNBT)tag));

        return new BrewInstance(taggedDuration, effects);
    }
}
