package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewAttributeEffect;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectType;
import net.minecraft.potion.Effects;
import net.minecraftforge.common.ForgeMod;

public class BrewEffectSwimSpeed extends BrewAttributeEffect
{
    public BrewEffectSwimSpeed(String translationKey, EffectType effectType, String uuid, AttributeModifier.Operation operation)
    {
        super(translationKey, effectType, ForgeMod.SWIM_SPEED, uuid, operation);
    }

    @Override
    protected double getModifierAmount(LivingEntity livingEntity, int potency) { return potency / 25d; }
}
