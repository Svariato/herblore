package com.gitlab.svariato.herblore.common.tile;

import com.gitlab.svariato.herblore.brew.BrewReagents;
import com.gitlab.svariato.herblore.brew.IBrewAttuningReagent;
import com.gitlab.svariato.herblore.brew.IBrewEffectReagent;
import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.block.BlockBrewmasterApparatus;
import com.gitlab.svariato.herblore.common.util.BrewHelper;
import com.gitlab.svariato.herblore.init.ModItems;
import com.gitlab.svariato.herblore.init.ModTiles;
import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class TileBrewmasterApparatus extends TileEntity implements ITickableTileEntity
{
    private static final int MAX_COOK_TIME = 720; // 9 stages (0%, 25%, 50%, 75%, 100%, 75%, 50%, 25%, 0%) of multiplier application.
    private static final int COOK_STAGE_DURATION = 80; // 4 seconds per stage.

    private List<ItemStack> inventory;

    /**
     * There are 8 stages for this tile.
     *
     * 0 - No items placed on it.
     * 1 - Glass Vial placed on it.
     * 2 - Glass Vial and one BrewEffectReagents placed on it.
     * 3 - Glass Vial and two BrewEffectReagents placed on it.
     * 4 - Glass Vial and three BrewEffectReagents placed on it.
     * 5 - Glass Vial, three BrewEffectReagents, and a BrewAttuningReagent placed on it.
     * 6 - Glass Vial, three BrewEffectReagents, a BrewAttuningReagent, and Fuel placed on it.
     * 7 - Glass Vial, three BrewEffectReagents, a BrewAttuningReagent, and Fuel placed on it. Lit with Flint and Steel.
     * 8 - Completed brew ready to be taken.
     */
    int stage;

    private int cookTime;

    public TileBrewmasterApparatus()
    {
        super(ModTiles.BREWMASTER_APPARATUS.get());

        inventory = new ArrayList<>(6);
        for (int i = 0; i < 6; i++) inventory.add(ItemStack.EMPTY);

        stage = 0;
        cookTime = 0;
    }

    @Override
    public void tick()
    {
        if (stage != 7 || world.isRemote) return;

        if (cookTime < MAX_COOK_TIME)
        {
            cookTime++;
            world.setBlockState(pos, getBlockState().with(BlockBrewmasterApparatus.AT_BEST_VALUES, isInSweetCookingSpot()));
        }

        if (cookTime >= MAX_COOK_TIME)
        {
            finishBrew();
        }
    }

    public ActionResultType attemptUse(BlockState state, PlayerEntity player, ItemStack stack)
    {
        switch (stage)
        {
            case 0:
                if (stack.getItem() == ModItems.GLASS_VIAL.get())
                {
                    copyShrinkAdd(stage, stack);
                    goToNextStage(state);
                    return ActionResultType.SUCCESS;
                }
                break;

            case 1: case 2: case 3:
                if (stack.getItem() instanceof IBrewEffectReagent)
                {
                    copyShrinkAdd(stage, stack);
                    goToNextStage(state);
                    return ActionResultType.SUCCESS;
                }
                break;

            case 4:
                if (stack.getItem() instanceof IBrewAttuningReagent)
                {
                    copyShrinkAdd(stage, stack);
                    goToNextStage(state);
                    return ActionResultType.SUCCESS;
                }
                break;

            case 5:
                if (stack.getItem() == Items.COAL)
                {
                    copyShrinkAdd(stage, stack);
                    goToNextStage(state);
                    return ActionResultType.SUCCESS;
                }
                break;

            case 6:
                if (stack.getItem() == Items.FLINT)
                {
                    goToNextStage(state);
                    return ActionResultType.SUCCESS;
                }
                break;

            case 7:
                finishBrew();
                break;

            case 8:
                ItemStack product = inventory.get(0);
                if (!player.inventory.addItemStackToInventory(product))
                {
                    player.dropItem(product, false);
                }
                inventory.set(0, ItemStack.EMPTY);
                goToNextStage(state);
                return ActionResultType.SUCCESS;
        }

        return ActionResultType.FAIL;
    }

    public void attemptClick(BlockState state, PlayerEntity player)
    {
        switch (stage)
        {
            case 1: case 2: case 3: case 4: case 5: case 6:
                if (!player.inventory.addItemStackToInventory(inventory.get(stage - 1)))
                {
                    player.dropItem(inventory.get(stage - 1), false);
                }
                inventory.set(stage - 1, ItemStack.EMPTY);
                goToPreviousStage(state);
                break;
        }
    }

    public void onDestroyBlock(World world)
    {
        for (ItemStack stack : inventory)
            world.addEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), stack));
    }

    @Override
    public void read(BlockState state, CompoundNBT nbt)
    {
        for (int i = 0; i < 5; i++)
            if (nbt.contains("Slot " + i))
                inventory.set(i, ItemStack.read(nbt.getCompound("Slot " + i)));

        super.read(state, nbt);
    }

    @Override
    public CompoundNBT write(CompoundNBT nbt)
    {
        for (int i = 0; i < 6; i++)
            nbt.put("Slot " + i, inventory.get(i).serializeNBT());

        return super.write(nbt);
    }

    private void finishBrew()
    {
        world.playSound(pos.getX() + 0.5d, pos.getY() + 0.5d, pos.getZ() + 0.5d, SoundEvents.BLOCK_LAVA_EXTINGUISH, SoundCategory.AMBIENT, 3f, 1f, false);
        goToNextStage(world.getBlockState(pos));
        inventory.set(0, BrewHelper.makeBrew(getMultiplierFromCookTimeRemaining(), new BrewReagents(inventory.get(4), inventory.get(1), inventory.get(2), inventory.get(3))));
        cookTime = 0;
        world.setBlockState(pos, getBlockState().with(BlockBrewmasterApparatus.AT_BEST_VALUES, false));
        for (int i = 1; i < 6; i++) inventory.set(i, ItemStack.EMPTY);
    }

    private void copyShrinkAdd(int slot, ItemStack stack)
    {
        ItemStack toAdd = stack.copy();
        toAdd.setCount(1);
        inventory.set(slot, toAdd);
        stack.shrink(1);
    }

    private void goToNextStage(BlockState state)
    {
        world.setBlockState(pos, world.getBlockState(pos).with(BlockBrewmasterApparatus.AT_BEST_VALUES, true));
        if (stage < 8) stage++;
        else stage = 0;
        world.setBlockState(pos, world.getBlockState(pos).with(BlockBrewmasterApparatus.STAGE, stage));
    }

    private void goToPreviousStage(BlockState state)
    {
        if (stage > 0)
        {
            stage--;
            world.setBlockState(pos, world.getBlockState(pos).with(BlockBrewmasterApparatus.STAGE, stage));
        }
    }

    private float getMultiplierFromCookTimeRemaining()
    {
        if (isInSweetCookingSpot())
        {
            return 1;
        }
        /*TODO: Can't decide between two equations for this. The first one would create a smooth 0-1 and remain at 1 for a full cook phase, while the second would
         *  smoothly grow to somewhere around 0.75 and swiftly jump to 1 for a full cook phase I've written both out here in that order, but will only be using one at a time. */

        /*

        FIRST ONE:

        float halfMaxCookTime = MAX_COOK_TIME / 2f;
        float halfCookStageDuration = COOK_STAGE_DURATION / 2f;
        return (-1f / (halfMaxCookTime - halfCookStageDuration)) * Math.abs(((float)cookTime) - halfMaxCookTime) + (halfMaxCookTime / (halfMaxCookTime - halfCookStageDuration));

        ------

        SECOND ONE:

        float halfMaxCookTime = MAX_COOK_TIME / 2f;
        return (-1f / halfMaxCookTime) * Math.abs(cookTime - halfMaxCookTime) + 1f;
        */
        else
        {
            float halfMaxCookTime = MAX_COOK_TIME / 2f;
            float halfCookStageDuration = COOK_STAGE_DURATION / 2f;
            return (-1f / (halfMaxCookTime - halfCookStageDuration)) * Math.abs(cookTime - halfMaxCookTime) + (halfMaxCookTime / (halfMaxCookTime - halfCookStageDuration));
        }
    }

    private boolean isInSweetCookingSpot()
    {
        return (float)cookTime >= MAX_COOK_TIME / 2f - COOK_STAGE_DURATION / 2f && (float)cookTime <= MAX_COOK_TIME / 2f + COOK_STAGE_DURATION / 2f; // "Sweet spot" lasts the full center stage;
    }
}
