package com.gitlab.svariato.herblore.common.capability;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.init.ModTags;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;

public class CapabilityPheromoneFollower
{
    @CapabilityInject(IPheromoneFollower.class)
    public static Capability<IPheromoneFollower> PHEROMONE_FOLLOWER_CAPABILITY = null;

    public static void register()
    {
        CapabilityManager.INSTANCE.register(IPheromoneFollower.class, new CapabilityStoragePheromoneFollower(), PheromoneFollower::new);
    }

    public static void onAttachCapabilitiesEvent(AttachCapabilitiesEvent<Entity> event)
    {
        Entity entity = event.getObject();
        if (entity instanceof AnimalEntity && entity.getType().isContained(ModTags.PHEROMONE_FOLLOWERS))
        {
            event.addCapability(new ResourceLocation(Herblore.MOD_ID, "pheromone_follower_capability"), new CapabilityProviderPheromoneFollower());
        }
    }
}
