package com.gitlab.svariato.herblore.common.item;

import com.gitlab.svariato.herblore.brew.IBrewAttuningReagent;
import net.minecraft.item.Item;

public class ItemBasicAttuningReagent extends Item implements IBrewAttuningReagent
{
    private final int brewColorContribution;
    private final float potencyMultiplier;
    private final float durationMultiplier;

    public ItemBasicAttuningReagent(Item.Properties properties, int brewColorContribution, float potencyMultiplier, float durationMultiplier)
    {
        super(properties);

        this.brewColorContribution = brewColorContribution;
        this.potencyMultiplier = potencyMultiplier;
        this.durationMultiplier = durationMultiplier;
    }

    @Override
    public int getBrewColorContribution() { return brewColorContribution; }

    @Override
    public float getPotencyMultiplier() { return potencyMultiplier; }

    @Override
    public float getDurationMultiplier() { return durationMultiplier; }
}
