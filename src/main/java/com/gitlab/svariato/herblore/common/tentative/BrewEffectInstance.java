package com.gitlab.svariato.herblore.common.tentative;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;

import java.util.function.Supplier;

public class BrewEffectInstance
{
    public static final int MAX_POTENCY = 100; // TODO: Maybe make this configurable, and/or relocate it?

    private final Supplier<BrewEffect> brewEffect;
    private final int potency;

    public BrewEffectInstance(Supplier<BrewEffect> brewEffect, int potency)
    {
        this.brewEffect = brewEffect;
        this.potency = Math.min(potency, MAX_POTENCY); // Enforce MAX_POTENCY value.
    }

    /**
     * Combine the effects of two BrewEffectInstances that share the same effect. Essentially
     * just combines their potency. Returns a new BrewEffectInstance that has the combined potency
     * of this BrewEffectInstance and the other BrewEffectInstance.
     */
    public BrewEffectInstance combine(BrewEffectInstance other)
    {
        return new BrewEffectInstance(brewEffect, potency + other.getPotency());
    }

    /**
     * Creates and returns an exact copy of this BrewEffectInstance.
     */
    public BrewEffectInstance cloneInstance()
    {
        return new BrewEffectInstance(brewEffect, potency);
    }

    public Supplier<BrewEffect> getBrewEffect() { return brewEffect; }

    public int getPotency() { return potency; }

    public CompoundNBT write(CompoundNBT nbt)
    {
        nbt.putString("Id", brewEffect.get().getRegistryName().toString());
        nbt.putInt("Potency", potency);
        return nbt;
    }

    public static BrewEffectInstance read(CompoundNBT nbt)
    {
        Supplier<BrewEffect> effect = BrewEffect.getBrewEffectFromRegistry(new ResourceLocation(nbt.getString("Id")));
        int potency = nbt.getInt("Potency");
        return new BrewEffectInstance(effect, potency);
    }
}
