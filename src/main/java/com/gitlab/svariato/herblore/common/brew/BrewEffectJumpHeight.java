package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.IBrewHandler;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.init.ModBrewEffects;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingEvent;

public class BrewEffectJumpHeight
{
    public static void onLivingJump(LivingEvent.LivingJumpEvent event)
    {
        LivingEntity livingEntity = event.getEntityLiving();
        IBrewHandler brewHandler = livingEntity.getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
        if (brewHandler != null && brewHandler.getActiveBrew() != null && brewHandler.getActiveBrew().getBrewEffectInstanceMap().hasBrewInstanceOfEffect(ModBrewEffects.JUMP_HEIGHT_BOOST))
        {
            BrewEffectInstance instance = brewHandler.getActiveBrew().getBrewEffectInstanceMap().getBrewInstanceOfEffect(ModBrewEffects.JUMP_HEIGHT_BOOST);
            livingEntity.setMotion(livingEntity.getMotion().add(0.0d, instance.getPotency() / 200d, 0.0d));
        }
    }
}
