package com.gitlab.svariato.herblore.common.capability;

import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.event.entity.living.LivingEvent;

import javax.annotation.Nullable;

public class BrewHandler implements IBrewHandler
{
    private static final String ACTIVE_BREW_INSTANCE_NBT_KEY = "herblore_active_brew_instance";

    private BrewInstance activeBrew = null;

    public BrewHandler() {}

    @Nullable
    public BrewInstance getActiveBrew()
    {
        return activeBrew;
    }

    public void setActiveBrew(BrewInstance brewInstance, LivingEntity livingEntity)
    {
        activeBrew = brewInstance.cloneInstance();
        for (BrewEffectInstance effectInstance : activeBrew.getBrewEffectInstanceMap().getBrewEffectInstances())
        {
            effectInstance.getBrewEffect().get().onActivated(livingEntity, effectInstance.getPotency(), activeBrew.getDuration());
        }
    }

    public void loadActiveBrew(BrewInstance brewInstance)
    {
        activeBrew = brewInstance.cloneInstance();
    }

    public void clearActiveBrew(LivingEntity livingEntity)
    {
        if (activeBrew == null) return;
        for (BrewEffectInstance effectInstance : activeBrew.getBrewEffectInstanceMap().getBrewEffectInstances())
            effectInstance.getBrewEffect().get().onCleared(livingEntity, effectInstance.getPotency(), activeBrew.getDuration());
        activeBrew = null;
    }

    public void tickActiveBrew(LivingEntity livingEntity)
    {
        if (activeBrew == null) return;

        activeBrew.decrementDuration();
        if (activeBrew.getDuration() == 0)
        {
            for (BrewEffectInstance effectInstance : activeBrew.getBrewEffectInstanceMap().getBrewEffectInstances())
                effectInstance.getBrewEffect().get().onExpired(livingEntity, effectInstance.getPotency());
            activeBrew = null;
        }
        else
        {
            for (BrewEffectInstance effectInstance : activeBrew.getBrewEffectInstanceMap().getBrewEffectInstances())
                effectInstance.getBrewEffect().get().tick(livingEntity, effectInstance.getPotency(), activeBrew.getDuration());
        }
    }

    // TODO: Maybe move this to a separate class?
    public static void onLivingEntityUpdate(LivingEvent.LivingUpdateEvent event)
    {
        LivingEntity livingEntity = event.getEntityLiving();
        IBrewHandler brewHandler = event.getEntityLiving().getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
        if (brewHandler != null) brewHandler.tickActiveBrew(livingEntity);
    }

    public CompoundNBT serializeNBT()
    {
        CompoundNBT nbt = new CompoundNBT();
        if (activeBrew != null) nbt.put(ACTIVE_BREW_INSTANCE_NBT_KEY, activeBrew.write(new CompoundNBT()));
        return nbt;
    }

    public void deserializeNBT(CompoundNBT nbt)
    {
        if (nbt.contains(ACTIVE_BREW_INSTANCE_NBT_KEY)) activeBrew = BrewInstance.read(nbt.getCompound(ACTIVE_BREW_INSTANCE_NBT_KEY));
        else activeBrew = null;
    }
}
