package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewAttributeEffect;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.potion.EffectType;

public class BrewEffectAttackSpeed extends BrewAttributeEffect
{
    public BrewEffectAttackSpeed(String name, EffectType effectType, String uuid, AttributeModifier.Operation operation)
    {
        super(name, effectType, () -> Attributes.ATTACK_SPEED, uuid, operation);
    }

    @Override
    protected double getModifierAmount(LivingEntity livingEntity, int potency)
    {
        return potency / 100f;
    }
}
