package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.capability.CapabilityPheromoneFollower;
import com.gitlab.svariato.herblore.common.capability.IPheromoneFollower;
import com.gitlab.svariato.herblore.common.goal.FollowPheromoneGoal;
import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.init.ModTags;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.GoalSelector;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.ai.goal.TemptGoal;
import net.minecraft.entity.passive.*;
import net.minecraft.potion.EffectType;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.util.List;
import java.util.Set;

public class BrewEffectPheromone extends BrewEffect
{
    private static final double VERTICAL_PHEROMONE_RADIUS = 3;
    private static final double MAX_HORIZONTAL_PHEROMONE_RADIUS = 20; // Max distance that the bounding box can expand to when it looks for animals to attract. TODO: Make this configurable?
    private static final int DEFAULT_GOAL_PRIORITY = 3; // TODO: Make this configurable?
    private static final int TICK_ACTION_FREQUENCY = 60;

    public BrewEffectPheromone()
    {
        super("pheromone", EffectType.BENEFICIAL);
    }

    @Override
    public void tick(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        super.tick(livingEntity, potency, durationRemaining);

        if (durationRemaining % TICK_ACTION_FREQUENCY == 0)
        {
            double radius = getPheromoneRadius(potency);
            List<Entity> entities = livingEntity.world.getEntitiesWithinAABBExcludingEntity(livingEntity, livingEntity.getBoundingBox().grow(radius, VERTICAL_PHEROMONE_RADIUS, radius));
            for (Entity entity : entities)
            {
                if (entity instanceof AnimalEntity)
                {
                    IPheromoneFollower pheromoneFollower = entity.getCapability(CapabilityPheromoneFollower.PHEROMONE_FOLLOWER_CAPABILITY).orElse(null);
                    if (pheromoneFollower != null) pheromoneFollower.setTarget(livingEntity);
                }
            }
        }
    }

    /**
     * Adds the FollowPheromoneGoal to every spawned AnimalEntity included in the pheromone_followers.json tag. If
     * that AnimalEntity already has a tempt goal, the priority of the FollowPheromoneGoal will use the same priority.
     * If not, the default priority is 3. // TODO: Make this configurable?
     */
    public static void onEntityJoinWorld(EntityJoinWorldEvent event)
    {
        Entity entity = event.getEntity();
        if (entity instanceof AnimalEntity && entity.getType().isContained(ModTags.PHEROMONE_FOLLOWERS))
        {
            AnimalEntity animalEntity = (AnimalEntity)entity;

            final Set<PrioritizedGoal> goals = ObfuscationReflectionHelper.getPrivateValue(GoalSelector.class, animalEntity.goalSelector, "field_220892_d");

            PrioritizedGoal foundTemptGoal = null;

            for (PrioritizedGoal prioritizedGoal : goals)
            {
                Goal goal = prioritizedGoal.getGoal();
                if (goal instanceof FollowPheromoneGoal) return; // Goal already exists on the AnimalEntity, so it doesn't need to be re-added. Exit function here.
                else if (foundTemptGoal == null && goal instanceof TemptGoal) foundTemptGoal = prioritizedGoal; // Store first TemptGoal found and use its priority if we add a FollowPheromoneGoal.
            }

            if (foundTemptGoal != null) animalEntity.goalSelector.addGoal(foundTemptGoal.getPriority(), new FollowPheromoneGoal(animalEntity, 1.25d, 20f));
            else animalEntity.goalSelector.addGoal(DEFAULT_GOAL_PRIORITY, new FollowPheromoneGoal(animalEntity, 1.25d, 20f));
        }
    }

    public static double getPheromoneRadius(int potency)
    {
        return potency * MAX_HORIZONTAL_PHEROMONE_RADIUS / BrewEffectInstance.MAX_POTENCY;
    }

    public static boolean isInRangeOfPheromone(AnimalEntity animalEntity, LivingEntity pheromoneTarget, int potency)
    {
        double radius = BrewEffectPheromone.getPheromoneRadius(potency);
        return pheromoneTarget.getBoundingBox().grow(radius, VERTICAL_PHEROMONE_RADIUS, radius).contains(animalEntity.getPositionVec());
    }
}
