package com.gitlab.svariato.herblore.common.capability;

import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

public class CapabilityStorageBrewHandler implements Capability.IStorage<IBrewHandler>
{
    @Override
    public INBT writeNBT(Capability<IBrewHandler> capability, IBrewHandler instance, Direction side)
    {
        return instance.serializeNBT();
    }

    @Override
    public void readNBT(Capability<IBrewHandler> capability, IBrewHandler instance, Direction side, INBT base)
    {
        CompoundNBT nbt = (CompoundNBT)base;
        instance.deserializeNBT(nbt);
    }
}
