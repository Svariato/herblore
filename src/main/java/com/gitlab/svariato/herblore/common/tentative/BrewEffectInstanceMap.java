package com.gitlab.svariato.herblore.common.tentative;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class BrewEffectInstanceMap
{
    private Map<Supplier<BrewEffect>, BrewEffectInstance> brewEffectInstances;

    public BrewEffectInstanceMap()
    {
        brewEffectInstances = new HashMap<>();
    }

    public BrewEffectInstance addBrewEffectInstance(BrewEffectInstance brewEffectInstance) { return brewEffectInstances.put(brewEffectInstance.getBrewEffect(), brewEffectInstance); }

    public BrewEffectInstance removeBrewEffect(Supplier<BrewEffect> brewEffect) { return brewEffectInstances.remove(brewEffect); }

    public Collection<BrewEffectInstance> getBrewEffectInstances() { return brewEffectInstances.values(); }

    public boolean hasBrewInstanceOfEffect(Supplier<BrewEffect> brewEffect)
    {
        return brewEffectInstances.containsKey(brewEffect);
    }

    @Nonnull
    public BrewEffectInstance getBrewInstanceOfEffect(Supplier<BrewEffect> brewEffect)
    {
        return brewEffectInstances.get(brewEffect);
    }
}
