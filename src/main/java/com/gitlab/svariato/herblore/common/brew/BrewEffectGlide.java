package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;

public class BrewEffectGlide extends BrewEffect
{
    public BrewEffectGlide()
    {
        super("glide", EffectType.BENEFICIAL);
    }

    @Override
    public void tick(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        super.tick(livingEntity, potency, durationRemaining);
        if (livingEntity.isCrouching())
        {
            livingEntity.setMotion(livingEntity.getMotion().x, livingEntity.getMotion().y * getFallSpeedMultiplier(potency), livingEntity.getMotion().z);
            livingEntity.fallDistance = 0;
        }
    }

    /**
     * Calculates the percentage potency (potency over max potency) and returns the corresponding value between 0
     * and 1. At max potency, returns 0.
     */
    private static double getFallSpeedMultiplier(int potency) { return potency * (-1d / BrewEffectInstance.MAX_POTENCY) + 1d; }
}
