package com.gitlab.svariato.herblore.common.item;

import com.gitlab.svariato.herblore.brew.Brew;
import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.IBrewHandler;
import com.gitlab.svariato.herblore.common.util.BrewHelper;
import com.gitlab.svariato.herblore.init.ModBrews;
import com.gitlab.svariato.herblore.init.ModItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

public class ItemBrew extends Item
{
    public ItemBrew(Item.Properties properties)
    {
        super(properties);
    }

    @Override
    public ItemStack getDefaultInstance() { return BrewHelper.setBrew(super.getDefaultInstance(), ModBrews.CUSTOM_BREW.get()); }

    @Override
    public int getUseDuration(ItemStack stack) { return BrewHelper.getBrew(stack).getDrinkTime(); }

    @Override
    public UseAction getUseAction(ItemStack stack) { return UseAction.DRINK; }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand)
    {
        player.setActiveHand(hand);
        return ActionResult.resultSuccess(player.getHeldItem(hand));
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World world, LivingEntity livingEntity)
    {
        Herblore.LOGGER.debug("Brew consumed.");
        IBrewHandler handler = livingEntity.getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
        if (handler != null)
        {
            Herblore.LOGGER.debug("Handler found from capability.");
            if (handler.getActiveBrew() != null)
            {
                Herblore.LOGGER.debug("Clearing currently-active brew.");
                handler.clearActiveBrew(livingEntity);
            }
            handler.setActiveBrew(BrewHelper.getTaggedBrewInstance(stack).cloneInstance(), livingEntity);
            Herblore.LOGGER.debug("Cloned brew instance set as active.");
        }

        if (livingEntity instanceof PlayerEntity)
        {
            PlayerEntity player = (PlayerEntity)livingEntity;
            if (!player.abilities.isCreativeMode) // Only take a dose if player is not in creative.
            {
                int doses = BrewHelper.getDosesRemaining(stack);
                if (doses == 1)
                {
                    ItemStack finishedStack = new ItemStack(ModItems.GLASS_VIAL.get());
                    if (!player.addItemStackToInventory(finishedStack))
                        player.dropItem(finishedStack, false);
                    return finishedStack;
                }
                else BrewHelper.setDosesRemaining(stack, doses - 1);
            }
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
    {
        BrewHelper.addBrewTooltip(stack, tooltip);
    }
}
