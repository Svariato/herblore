package com.gitlab.svariato.herblore.common.tentative;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.init.ModBrewEffects;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.function.Supplier;

public class BrewEffect extends ForgeRegistryEntry<BrewEffect>
{
    private final String translationKey;
    private final EffectType effectType;

    public BrewEffect(String translationName, EffectType effectType)
    {
        this.translationKey = "brew_effect." + Herblore.MOD_ID + "." + translationName;
        this.effectType = effectType;
    }

    public String getTranslationKey() { return translationKey; }

    public EffectType getEffectType() { return effectType; }

    public void onActivated(LivingEntity livingEntity, int potency, int durationRemaining) {}

    public void onCleared(LivingEntity livingEntity, int potency, int durationRemaining) {}

    public void onExpired(LivingEntity livingEntity, int potency) {}

    public void tick(LivingEntity livingEntity, int potency, int durationRemaining) {}

    public static Supplier<BrewEffect> getBrewEffectFromRegistry(ResourceLocation name)
    {
        for (RegistryObject<BrewEffect> registeredEffect : ModBrewEffects.BREW_EFFECT_REGISTER.getEntries())
        {
            if (name.equals(registeredEffect.get().getRegistryName()))
            {
                return registeredEffect;
            }
        }

        return null;
    }
}
