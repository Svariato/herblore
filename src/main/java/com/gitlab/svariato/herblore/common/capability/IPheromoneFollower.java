package com.gitlab.svariato.herblore.common.capability;

import net.minecraft.entity.LivingEntity;

public interface IPheromoneFollower
{
    LivingEntity getTarget();

    void setTarget(LivingEntity newTarget);
}
