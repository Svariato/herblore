package com.gitlab.svariato.herblore.common.capability;

import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nullable;


public interface IBrewHandler extends INBTSerializable<CompoundNBT>
{
    @Nullable
    BrewInstance getActiveBrew();

    void setActiveBrew(BrewInstance effectInstance, LivingEntity livingEntity);

    void loadActiveBrew(BrewInstance effectInstance);

    void clearActiveBrew(LivingEntity livingEntity);

    void tickActiveBrew(LivingEntity livingEntity);
}
