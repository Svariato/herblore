package com.gitlab.svariato.herblore.common.world;

import com.gitlab.svariato.herblore.init.ModBlocks;
import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Blocks;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.placement.*;
import net.minecraftforge.registries.ForgeRegistries;

public class ModWorldGeneration
{
    public static final BlockClusterFeatureConfig OAKBLOOD_PATCH_CONFIG = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(ModBlocks.OAKBLOOD_PLANT.get().getDefaultState()), new SimpleBlockPlacer())).tries(16).xSpread(3).zSpread(3).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK.getBlock())).func_227317_b_().build();

    public static final BlockClusterFeatureConfig ARROWHEAD_LEAVES_PATCH_CONFIG = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(ModBlocks.ARROWHEAD_LEAVES_PLANT.get().getDefaultState()), new SimpleBlockPlacer())).tries(32).xSpread(2).zSpread(2).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK.getBlock())).func_227317_b_().build();

    /**
     * COUNT_HEIGHTMAP - Takes FrequencyConfig with a count parameter that determines the number of FeatureConfig executions per chunk.
     * COUNT_CHANCE_HEIGHTMAP = Takes HeightWithChanceConfig with a count parameter that determines the number of FeatureConfig execution attempts
     *                          per chunk and a chance parameter with that determines the chance that a given execution attempt is successful.
     */

    public static void addWorldGen()
    {
        for (Biome biome : ForgeRegistries.BIOMES)
        {
            if (biome == Biomes.DARK_FOREST || biome == Biomes.DARK_FOREST_HILLS)
            {
                biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(OAKBLOOD_PATCH_CONFIG).withPlacement(Placement.COUNT_CHANCE_HEIGHTMAP.configure(new HeightWithChanceConfig(1, 0.1f))));
            }

            if (biome == Biomes.PLAINS)
            {
                biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(ARROWHEAD_LEAVES_PATCH_CONFIG).withPlacement(Placement.COUNT_CHANCE_HEIGHTMAP.configure(new HeightWithChanceConfig(1, 0.1f))));
            }
        }
    }
}
