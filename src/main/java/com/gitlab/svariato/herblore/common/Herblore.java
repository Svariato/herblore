package com.gitlab.svariato.herblore.common;

import com.gitlab.svariato.herblore.common.brew.BrewEffectJumpHeight;
import com.gitlab.svariato.herblore.common.brew.BrewEffectKnockbackResistance;
import com.gitlab.svariato.herblore.common.brew.BrewEffectLifeSteal;
import com.gitlab.svariato.herblore.common.brew.BrewEffectPheromone;
import com.gitlab.svariato.herblore.common.capability.BrewHandler;
import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.CapabilityPheromoneFollower;
import com.gitlab.svariato.herblore.common.world.ModWorldGeneration;
import com.gitlab.svariato.herblore.init.*;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.entity.Entity;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;

@Mod(Herblore.MOD_ID)
public class Herblore
{
    public static final String MOD_ID = "herblore";

    public static final Logger LOGGER = LogManager.getLogger();
    public static final boolean DEBUG = true;

    public Herblore()
    {
        LOGGER.debug("RandomUUID: " + UUID.randomUUID().toString());

        IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();

        modBus.addListener(Herblore::onCommonSetupEvent);
        modBus.addListener(Herblore::onClientSetupEvent);
        modBus.addListener(ModRegistries::createRegistries);
        modBus.addListener(ModItemColors::onLoadComplete);

        ModItems.ITEM_REGISTER.register(modBus);
        ModBlocks.BLOCK_REGISTER.register(modBus);
        ModTiles.TILE_REGISTER.register(modBus);
        ModBrews.BREW_REGISTER.register(modBus);
        ModBrewEffects.BREW_EFFECT_REGISTER.register(modBus);

        MinecraftForge.EVENT_BUS.addGenericListener(Entity.class, CapabilityBrewHandler::onAttachCapabilitiesEvent);
        MinecraftForge.EVENT_BUS.addGenericListener(Entity.class, CapabilityPheromoneFollower::onAttachCapabilitiesEvent);

        MinecraftForge.EVENT_BUS.addListener(BrewHandler::onLivingEntityUpdate);

        MinecraftForge.EVENT_BUS.addListener(BrewEffectJumpHeight::onLivingJump);
        MinecraftForge.EVENT_BUS.addListener(BrewEffectLifeSteal::onLivingAttack);
        MinecraftForge.EVENT_BUS.addListener(BrewEffectKnockbackResistance::onLivingKnockback);
        MinecraftForge.EVENT_BUS.addListener(BrewEffectPheromone::onEntityJoinWorld);
    }

    public static void onCommonSetupEvent(FMLCommonSetupEvent event)
    {
        CapabilityBrewHandler.register();
        CapabilityPheromoneFollower.register();

        DeferredWorkQueue.runLater(ModWorldGeneration::addWorldGen);
    }

    public static void onClientSetupEvent(FMLClientSetupEvent event)
    {
        RenderTypeLookup.setRenderLayer(ModBlocks.OAKBLOOD_PLANT.get(), RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.ARROWHEAD_LEAVES_PLANT.get(), RenderType.getCutout());
    }
}
