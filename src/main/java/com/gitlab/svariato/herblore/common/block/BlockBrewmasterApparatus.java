package com.gitlab.svariato.herblore.common.block;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.tile.TileBrewmasterApparatus;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.Random;

public class BlockBrewmasterApparatus extends Block
{
    public static final IntegerProperty STAGE = IntegerProperty.create("stage", 0, 8);
    public static final BooleanProperty AT_BEST_VALUES = BooleanProperty.create("at_best_values");

    public BlockBrewmasterApparatus()
    {
        super(Block.Properties.create(Material.IRON).sound(SoundType.METAL).hardnessAndResistance(2.0f).notSolid());
        this.setDefaultState(this.getStateContainer().getBaseState().with(STAGE, 0).with(AT_BEST_VALUES, false));
    }

    @Override
    public boolean hasTileEntity(BlockState state) { return true; }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) { return new TileBrewmasterApparatus(); }

    @Override
    public void animateTick(BlockState state, World world, BlockPos pos, Random rand)
    {
        if (state.get(STAGE) == 7)
        {
            double blockPosX = pos.getX();
            double blockPosY = pos.getY();
            double blockPosZ = pos.getZ();

            // Fire particle spawn.
            double fireParticleSpawnX = blockPosX + 0.5d + nextDoubleInRange(rand, -0.05d, 0.05d);
            double fireParticleSpawnY = blockPosY + 0.5d;
            double fireParticleSpawnZ = blockPosZ + 0.5d + nextDoubleInRange(rand, -0.05d, 0.05d);
            world.addParticle(ParticleTypes.FLAME, fireParticleSpawnX, fireParticleSpawnY, fireParticleSpawnZ, 0, 0.0075, 0);

            // Bubble particle spawn when this brew is at its best.
            if (state.get(AT_BEST_VALUES))
            {
                if (rand.nextDouble() < 0.4d)
                    world.playSound(blockPosX + 0.5d, blockPosY + 0.8d, blockPosZ + 0.5d, SoundEvents.BLOCK_BUBBLE_COLUMN_BUBBLE_POP, SoundCategory.AMBIENT, 2.5f, 0.5f, false);

                double bubbleParticleSpawnX = blockPosX + 0.5d + nextDoubleInRange(rand, -0.02d, 0.02d);
                double bubbleParticleSpawnY = blockPosY + 1.3d + nextDoubleInRange(rand, -0.1, 0.2);
                double bubbleParticleSpawnZ = blockPosZ + 0.5d + nextDoubleInRange(rand, -0.02d, 0.02d);
                world.addParticle(ParticleTypes.BUBBLE_POP, bubbleParticleSpawnX, bubbleParticleSpawnY, bubbleParticleSpawnZ, 0, 0, 0);
            }
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit)
    {
        if (!world.isRemote)
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity instanceof TileBrewmasterApparatus)
            {
                return ((TileBrewmasterApparatus) tileEntity).attemptUse(state, player, player.getHeldItem(hand));
            }
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onBlockClicked(BlockState state, World world, BlockPos pos, PlayerEntity player)
    {
        if (!world.isRemote && player.getHeldItem(Hand.MAIN_HAND) == ItemStack.EMPTY)
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity instanceof TileBrewmasterApparatus)
            {
                ((TileBrewmasterApparatus)tileEntity).attemptClick(state, player);
            }
        }
    }

    @Override
    public void fillStateContainer(StateContainer.Builder builder)
    {
        super.fillStateContainer(builder);
        builder.add(STAGE);
        builder.add(AT_BEST_VALUES);
    }

    @Override
    public void onBlockHarvested(World world, BlockPos pos, BlockState state, PlayerEntity player)
    {
        TileEntity tileEntity = world.getTileEntity(pos);
        if (tileEntity instanceof TileBrewmasterApparatus)
        {
            ((TileBrewmasterApparatus)tileEntity).onDestroyBlock(world);
        }
    }

    private static double nextDoubleInRange(Random rand, double min, double max)
    {
        return min + (max - min) * rand.nextDouble();
    }
}
