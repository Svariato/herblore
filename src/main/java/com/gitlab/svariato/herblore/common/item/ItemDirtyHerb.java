package com.gitlab.svariato.herblore.common.item;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

import java.util.function.Supplier;

public class ItemDirtyHerb extends Item
{
    protected final Supplier<Item> cleanHerbSupplier;

    public ItemDirtyHerb(Item.Properties properties, Supplier<Item> cleanHerbSupplier)
    {
        super(properties);
        this.cleanHerbSupplier = cleanHerbSupplier;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand)
    {
        ItemStack stack = player.getHeldItem(hand);

        BlockRayTraceResult rayTraceResult = rayTrace(world, player, RayTraceContext.FluidMode.SOURCE_ONLY);

        if (rayTraceResult.getType() == RayTraceResult.Type.BLOCK && world.getBlockState(rayTraceResult.getPos()).getBlock() == Blocks.WATER)
        {
            ItemStack resultStack = turnDirtyIntoClean(stack, player, new ItemStack(cleanHerbSupplier.get()));
            return world.isRemote() ? ActionResult.resultSuccess(resultStack) : ActionResult.resultConsume(resultStack);
        }

        return ActionResult.resultPass(stack);
    }

    // Helper function for use in onItemRightClick. Probably shouldn't be called anywhere else.
    private ItemStack turnDirtyIntoClean(ItemStack dirtyStack, PlayerEntity player, ItemStack cleanStack)
    {
        dirtyStack.shrink(1);
        player.addStat(Stats.ITEM_USED.get(this));
        if (dirtyStack.isEmpty()) return cleanStack;
        else
        {
            if (!player.inventory.addItemStackToInventory(cleanStack))
            {
                player.dropItem(cleanStack, false);
            }

            return dirtyStack;
        }
    }
}
