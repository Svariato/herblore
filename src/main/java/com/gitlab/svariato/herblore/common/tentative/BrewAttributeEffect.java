package com.gitlab.svariato.herblore.common.tentative;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.potion.EffectType;

import java.util.UUID;
import java.util.function.Supplier;

public abstract class BrewAttributeEffect extends BrewEffect
{
    protected final Supplier<Attribute> attribute;
    private final UUID uuid;
    private AttributeModifier.Operation operation;

    public BrewAttributeEffect(String translationName, EffectType effectType, Supplier<Attribute> attribute, String uuid, AttributeModifier.Operation operation)
    {
        super(translationName, effectType);
        this.attribute = attribute;
        this.uuid = UUID.fromString(uuid);
        this.operation = operation;
    }

    @Override
    public void onActivated(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        super.onActivated(livingEntity, potency, durationRemaining);
        applyAttributeModifier(livingEntity, getModifierAmount(livingEntity, potency));
    }

    @Override
    public void onCleared(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        super.onCleared(livingEntity, potency, durationRemaining);
        removeAttributeModifier(livingEntity);
    }

    @Override
    public void onExpired(LivingEntity livingEntity, int potency)
    {
        super.onExpired(livingEntity, potency);
        removeAttributeModifier(livingEntity);
    }

    protected abstract double getModifierAmount(LivingEntity livingEntity, int potency);

    private void applyAttributeModifier(LivingEntity livingEntity, double amount)
    {
        AttributeModifier modifier = new AttributeModifier(uuid, getTranslationKey(), amount, operation);
        ModifiableAttributeInstance mai = livingEntity.getAttributeManager().createInstanceIfAbsent(attribute.get());
        if (mai != null)
        {
            mai.removeModifier(modifier);
            mai.applyNonPersistentModifier(modifier);
        }
    }

    private void removeAttributeModifier(LivingEntity livingEntity)
    {
        ModifiableAttributeInstance mai = livingEntity.getAttributeManager().createInstanceIfAbsent(attribute.get());
        if (mai != null)
        {
            mai.removeModifier(uuid);
        }
    }
}
