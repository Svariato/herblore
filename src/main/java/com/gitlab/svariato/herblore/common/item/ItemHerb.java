package com.gitlab.svariato.herblore.common.item;

import com.gitlab.svariato.herblore.brew.IBrewEffectReagent;
import com.gitlab.svariato.herblore.common.tentative.BrewInstance;
import net.minecraft.item.Item;

public class ItemHerb extends Item implements IBrewEffectReagent
{
    private final int brewColorContribution;
    private final BrewInstance baseBrewInstance;

    public ItemHerb(Item.Properties properties, int brewColorContribution, BrewInstance baseBrewInstance)
    {
        super(properties);
        this.brewColorContribution = brewColorContribution;
        this.baseBrewInstance = baseBrewInstance;
    }

    public int getBrewColorContribution() { return brewColorContribution; }

    public BrewInstance getBaseBrewInstance() { return baseBrewInstance; }
}
