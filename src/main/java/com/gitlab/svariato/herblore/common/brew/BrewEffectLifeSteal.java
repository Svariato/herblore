package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.capability.CapabilityBrewHandler;
import com.gitlab.svariato.herblore.common.capability.IBrewHandler;
import com.gitlab.svariato.herblore.common.tentative.BrewEffectInstance;
import com.gitlab.svariato.herblore.init.ModBrewEffects;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class BrewEffectLifeSteal
{
    public static void onLivingAttack(LivingAttackEvent event)
    {
        Entity sourceEntity = event.getSource().getImmediateSource();
        if (sourceEntity instanceof LivingEntity)
        {
            LivingEntity sourceLivingEntity = (LivingEntity)sourceEntity;
            IBrewHandler brewHandler = sourceLivingEntity.getCapability(CapabilityBrewHandler.BREW_HANDLER_CAPABILITY).orElse(null);
            if (brewHandler != null && brewHandler.getActiveBrew() != null && brewHandler.getActiveBrew().getBrewEffectInstanceMap().hasBrewInstanceOfEffect(ModBrewEffects.LIFE_STEAL))
            {
                BrewEffectInstance instance = brewHandler.getActiveBrew().getBrewEffectInstanceMap().getBrewInstanceOfEffect(ModBrewEffects.LIFE_STEAL);
                sourceLivingEntity.heal(instance.getPotency() / 500f * event.getAmount());
            }
        }
    }
}
