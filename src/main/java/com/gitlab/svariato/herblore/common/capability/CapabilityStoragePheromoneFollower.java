package com.gitlab.svariato.herblore.common.capability;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class CapabilityStoragePheromoneFollower implements Capability.IStorage<IPheromoneFollower>
{

    @Nullable
    @Override
    public INBT writeNBT(Capability<IPheromoneFollower> capability, IPheromoneFollower instance, Direction side)
    {
        return null;
    }

    @Override
    public void readNBT(Capability<IPheromoneFollower> capability, IPheromoneFollower instance, Direction side, INBT nbt)
    {

    }
}
