package com.gitlab.svariato.herblore.common.brew;

import com.gitlab.svariato.herblore.common.Herblore;
import com.gitlab.svariato.herblore.common.tentative.BrewEffect;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectType;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

public class BrewEffectHealthRegeneration extends BrewEffect
{
    // TODO: Add a constructor that can modify the healing tick rate. Reflect that parameter in tick().

    public BrewEffectHealthRegeneration()
    {
        super("health_regeneration", EffectType.BENEFICIAL);
    }

    @Override
    public void onActivated(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        Herblore.LOGGER.debug("Regeneration effect activated!");
    }

    @Override
    public void onCleared(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        Herblore.LOGGER.debug("Regeneration effect cleared!");
    }

    @Override
    public void onExpired(LivingEntity livingEntity, int potency)
    {
        Herblore.LOGGER.debug("Regeneration effect expired!");
    }

    @Override
    public void tick(LivingEntity livingEntity, int potency, int durationRemaining)
    {
        if (durationRemaining % 20 == 0)
        {
            Herblore.LOGGER.debug("Ticking regeneration effect every twentieth tick! Duration remaining: " + durationRemaining);
            float healAmount = potency / 100f * 10f;
            Herblore.LOGGER.debug("Healing for: " + healAmount);
            livingEntity.heal(healAmount);
        }
    }
}
